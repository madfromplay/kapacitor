package kapacitor

import (
	"sync"
	"time"
)

type Kapacitor struct {
	mu        sync.Mutex
	period    time.Duration
	bucket    []interface{}
	Output    chan []interface{}
	Testfield string
}

func (kap *Kapacitor) SetPeriod(period int64) {
	kap.mu.Lock()
	defer kap.mu.Unlock()
	kap.period = time.Duration(period) * time.Millisecond
}

func (kap *Kapacitor) AddIterm(item interface{}) {
	kap.mu.Lock()
	defer kap.mu.Unlock()
	kap.bucket = append(kap.bucket, item)
}

func (kap *Kapacitor) dump() {
	ticker := time.NewTicker(kap.period)
	for {
		<-ticker.C
		kap.mu.Lock()
		if len(kap.bucket) > 0 {
			kap.Output <- kap.bucket
			kap.bucket = nil
		}
		kap.mu.Unlock()
	}
}

func NewKapacitor(period int64) *Kapacitor {
	var kap Kapacitor
	if period == 0 {
		kap.SetPeriod(5000)
	} else {
		kap.SetPeriod(period)
	}
	kap.Output = make(chan []interface{})
	go kap.dump()
	return &kap
}
